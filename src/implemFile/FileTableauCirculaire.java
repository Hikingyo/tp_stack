package implemFile;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;

public class FileTableauCirculaire implements Queue<Integer> {

	private int head;
	private int length;
	private Integer[] stack;
	private int capacity;

	public FileTableauCirculaire() {
		this.head = 0;
		this.length = 0;
		this.capacity = 1000;
		this.stack = new Integer[this.capacity];
	}

	public FileTableauCirculaire(int capacity) {
		this.head = 0;
		this.length = 0;
		this.capacity = capacity;
		this.stack = new Integer[this.capacity];
	}

	@Override
	public boolean addAll(Collection<? extends Integer> arg0) throws IllegalStateException {
		if (arg0.size() + length > capacity) {
			throw new IllegalStateException("La file ne peut accueillir la collection");
		} else {
			for (Integer i : arg0) {
				this.add(i);
			}
			return true;
		}
	}

	@Override
	public void clear() {
		this.length = 0;
	}

	@Override
	public boolean contains(Object arg0) throws NullPointerException {
		if (arg0 == null) {
			throw new NullPointerException();
		}
		for (int i = this.head; i < this.head + this.length; i++) {
			if (this.stack[i % this.capacity].equals(arg0)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		for(Object i : arg0){
			if(!this.contains(i)){
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isEmpty() {
		if (this.length == 0) {
			return true;
		}
		return false;
	}

	@Override
	public Iterator<Integer> iterator() {

		class Iter implements Iterator<Integer> {
			private final FileTableauCirculaire stack;
			private int current;

			Iter(FileTableauCirculaire stack) {
				this.stack = stack;
				this.current = stack.getHead();
			}

			@Override
			public boolean hasNext() {
				return current < stack.getHead() + stack.getLength();
			}

			@Override
			public Integer next() {
				int res = stack.getValue(current % capacity);
				current++;
				return res;
			}
		}
		return new Iter(this);
	}

	@Override
	public boolean remove(Object arg0) {
		if (arg0.equals(null)) {
			throw new NullPointerException();
		}
		if (!this.contains(arg0)) {
			return false;
		}
		int start = this.head;
		for (int i = start; i < start + this.length; i++) {
			if (!this.stack[i % capacity].equals(arg0)) {
				this.add(this.stack[i % capacity]);
			}
			this.length--;
			this.head++;
		}
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> arg0) throws NullPointerException {
		if (arg0 == null) {
			throw new NullPointerException();
		}
		for (Object i : arg0) {
			if (i == null) {
				throw new NullPointerException();
			}
			this.remove(i);
		}
		return true;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		if(arg0.equals(null) || arg0.contains(null)){
			throw new NullPointerException();
		}
		int start = this.head;
		for (int i = start; i < start + this.length; i++) {
			if (arg0.contains(this.stack[i % capacity])) {
				this.add(this.stack[i % capacity]);
			}
			this.length--;
			this.head++;
		}
		return true;
	}

	@Override
	public int size() {
		return this.length;
	}

	@Override
	public Object[] toArray() {
		Object[] res = new Object[this.length];
		int index = 0;
		for (int i = this.head; i < this.head + this.length; i++) {
			res[index] = this.stack[i % capacity];
			index++;
		}
		return res;
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		T[] t;
		if(arg0.length < this.length){
			Integer[] t1 = new Integer[this.length];
			t = (T[])t1;
		}
		else{
			t = arg0;
		}
		int index = 0;
		for (int i = this.head; i < this.head + this.length; i++) {
			t[index] = (T)this.stack[i % capacity];
			index++;
		}
		return t;
	}

	@Override
	public boolean add(Integer arg0) throws IllegalStateException {
		if (this.length + 1 > this.capacity) {
			throw new IllegalStateException("Capacit� de la file d�pass�e");
		}
		return this.offer(arg0);
	}

	@Override
	public Integer element() throws NoSuchElementException {
		if (this.length == 0) {
			throw new NoSuchElementException();
		}
		return this.peek();
	}

	@Override
	public boolean offer(Integer arg0) {
		if(arg0.equals(null)){
			throw new NullPointerException();
		}
		if(this.length + 1 <= this.capacity){

			stack[(this.head + this.length) % capacity] = arg0;
			this.length++;
			return true;
		}
		return false;
	}

	@Override
	public Integer peek() {
		if (this.length == 0)
			return null;
		else
			return this.stack[this.head];
	}

	@Override
	public Integer poll() {
		if (this.length == 0)
			return null;
		else
			return this.remove();
	}

	@Override
	public Integer remove() throws NoSuchElementException {
		if (this.length == 0) {
			throw new NoSuchElementException();
		}
		Integer res = this.stack[this.head];
		this.head = ++this.head % this.capacity;
		this.length = this.length - 1;
		return res;
	}

	public int getHead() {
		return this.head;
	}

	public int getLength() {
		return this.length;
	}
	
	private int getValue(int i ){
		return this.stack[i];
	}
}
